<?php namespace Modules\Test\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class InArrayController extends Controller {


	public function index()
	{
        $result = false;
        $testArr = ['abc','cde'];
        $test = 'abc';
        array_walk_recursive($testArr,function(&$val,$key) use(&$result,$test){
            if($val == $test) {
                $result = $key;
            }
        });
        array_flip();
		return view('test::index');
	}
	
}